﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Assignment_2_WDT.Login" %>

<!DOCTYPE html>
<html lang="en">
<head id="Head1" runat="server">
    <title>Admin tools</title>
    <%-- Style Section --%>
    <link rel="stylesheet" type="text/css" href="~/Content/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="~/Content/Site.css">
    <link rel="stylesheet" type="text/css" href="~/Content/bootstrap.min.css" />
    <script src="~/Scripts/modernizr-2.6.2.js"></script>
</head>
<body>

    <!-- Static navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="#">Admin</a>
            </div>

            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="Login.aspx">Login</a></li>
                </ul>
            </div>

            <!--/.nav-collapse -->
        </div>
        <!--/.container -->
    </div>

    <!-- Content Page Section -->
    <div class="container body-content">
        <form id="form1" runat="server">
            <asp:Login ID="AdminLogin" runat="server" OnAuthenticate="ValidateUser"></asp:Login>
        </form>
    </div>
</body>
</html>

