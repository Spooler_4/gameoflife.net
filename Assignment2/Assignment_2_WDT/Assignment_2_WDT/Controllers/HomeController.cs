﻿using Assignment_2_WDT.Attributes;
using Assignment_2_WDT.Models;
using CryptSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_2_WDT.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            List<GameOfLife> ActiveGames = new List<GameOfLife>();
            Session["ActiveGames"] = ActiveGames;
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        //POST: post, user/login
        [HttpPost]
        [ActionName("Login")]
        public ActionResult LoginPost([Bind(Include = "Email,Password")] User user)
        {
            /* UsersEntities is the name provided when you created your model
             * if you changed yours during the creation of the model, then you 
             * will find the name of the entity in your Web.config file in the 
             * connection string. This object represents your database.*/
            using (A2WDTDBEntities1 db = new A2WDTDBEntities1())
            {
                // Retrieve a user with the same username and password.
                User login = db.User.FirstOrDefault(u => u.Email == user.Email);

                // If successful set the session variables and go to Member page.
                if (login != null && Crypter.CheckPassword(user.Password, login.Password))
                {

                    Session["UserID"] = login.UserID;
                    Session["Email"] = login.Email;
                    Session["Name"] = login.FirstName + " " + login.LastName;
                    return RedirectToAction("Index");
                }

            }

            // Otherwise return them to the login page

            return View(user);
        }

        public ActionResult Logout()
        {
            // Kill the session variables & return to login page

            Session.Clear();

            return RedirectToAction("Login");
        }

        private A2WDTDBEntities1 db = new A2WDTDBEntities1();

        // POST: User Registration
        /* Sets IsAdmin and crypts password - then creates users based on input.
         */
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "UserID,Email,Password,FirstName,LastName")] User user)
        {
            user.IsAdmin = false;
            user.Password = Crypter.Blowfish.Crypt(user.Password);

            if (ModelState.IsValid)
            {
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }







}