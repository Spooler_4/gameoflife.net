﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_2_WDT.Models;
using Assignment_2_WDT.Attributes;

namespace Assignment_2_WDT.Controllers
{
    public class UserGamesController : Controller
    {
        private A2WDTDBEntities1 db = new A2WDTDBEntities1();

        public ActionResult ActiveGames()
        {
            var gm = (List<GameOfLife>)Session["ActiveGames"];
            GameOfLife[] games = gm.ToArray();
            ViewData["Games"] = games;
            return View();
        }

        public ActionResult SaveGame()
        {
            GameOfLife game = (GameOfLife)Session["GameOfLife"];
            string UserID = Session["UserID"].ToString();
            string Name = Session["Name"].ToString();
            int Height = game.Height;
            int Width = game.Width;
            string Cells = game.ToPlainString();
            UserGame last = db.UserGames.Last();
            int newID = last.UserGameID + 1;
            UserGame userGame = new UserGame(newID, UserID, Name, Height, Width, Cells);
            if (ModelState.IsValid)
            {
                db.UserGames.Add(userGame);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        //GET: Start the game already !
        public Object PlayGame()
        {

            GameOfLife gameOfLife = (GameOfLife)Session["GameOfLife"];
            string convertedCells = gameOfLife.ToString();
            ViewData["Cells"] = convertedCells;
            return View();
        }

        public ActionResult PlayActiveGame(GameOfLife game)
        {
            Session["GameOfLife"] = game;
            string convertedCells = game.ToString();
            ViewData["Cells"] = convertedCells;
            return View();
        }

        //GET: Start the game already !
        public Object takeTurn()
        {
            GameOfLife gameOfLife = (GameOfLife)Session["GameOfLife"];
            gameOfLife.TakeTurn();
            return gameOfLife;
        }


        // GET: UserGames
        public ActionResult Index()
        {
            var userGames = db.UserGames.Include(u => u.User);
            return View(userGames.ToList());
        }

        // GET: UserGames/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserGame userGame = db.UserGames.Find(id);
            if (userGame == null)
            {
                return HttpNotFound();
            }
            return View(userGame);
        }

        // GET: UserGames/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.User, "UserID", "Email");
            return View();
        }

        // POST: UserGames/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserGameID,UserID,Name,Height,Width,Cells")] UserGame userGame)
        {
            if (ModelState.IsValid)
            {
                db.UserGames.Add(userGame);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.User, "UserID", "Email", userGame.UserID);
            return View(userGame);
        }

        // GET: UserGames/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserGame userGame = db.UserGames.Find(id);
            if (userGame == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.User, "UserID", "Email", userGame.UserID);
            return View(userGame);
        }

        // POST: UserGames/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserGameID,UserID,Name,Height,Width,Cells")] UserGame userGame)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userGame).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.User, "UserID", "Email", userGame.UserID);
            return View(userGame);
        }

        // GET: UserGames/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserGame userGame = db.UserGames.Find(id);
            if (userGame == null)
            {
                return HttpNotFound();
            }
            return View(userGame);
        }

        // POST: UserGames/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserGame userGame = db.UserGames.Find(id);
            db.UserGames.Remove(userGame);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
