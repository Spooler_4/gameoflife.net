﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment_2_WDT.Models;
using Assignment_2_WDT.Attributes;
using System.Text.RegularExpressions;

namespace Assignment_2_WDT.Controllers
{
    public class UserTemplatesController : Controller
    {
        private A2WDTDBEntities1 db = new A2WDTDBEntities1();
        public enum Cell
        {
            Alive,
            Dead
        }

        // GET: UserTemplates
        public ActionResult Index(string searchString)
        {
            var userTemplates = db.UserTemplates.Include(u => u.User);
            // if a SearchString has been entered this filters the
            // results returned by comparing string with Tempalte Name
            if (!String.IsNullOrEmpty(searchString))
            {
                userTemplates = userTemplates.Where
                    (UserTemplate => UserTemplate.Name.Contains(searchString));
            }
            return View(userTemplates.ToList());
        }

        // GET: UserTemplates/Details
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }
            return View(userTemplate);
        }

        // GET: UserTemplates/MyTemplates
        public ActionResult MyTemplates(string searchString)
        {
            var userTemplates = db.UserTemplates.Include(u => u.User);
            // if a SearchString has been entered this filters the
            // results returned by comparing string with Tempalte Name
            if (!String.IsNullOrEmpty(searchString))
            {
                userTemplates = userTemplates.Where
                    (UserTemplate => UserTemplate.Name.Contains(searchString));
            }
            return View(userTemplates.ToList());
        }

        // GET: UserTemplates/Create
        [CustomAuthorize]
        public ActionResult Create()
        {

            ViewBag.UserID = new SelectList(db.User, "UserID", "Email");
            return View();
        }

        // POST: UserTemplates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create([Bind(Include = "UserTemplateID,UserID,Name,Height,Width,Cells")] UserTemplate userTemplate)
        {
            if (ModelState.IsValid)
            {

                db.UserTemplates.Add(userTemplate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.User, "UserID", "Email", userTemplate.UserID);
            return View(userTemplate);
        }

        // GET: UserTemplates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }
            return View(userTemplate);
        }

        // POST: UserTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            db.UserTemplates.Remove(userTemplate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // Create active game MOVE TO UserGames
        public ActionResult CreateActiveGame(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }
            SetGame(userTemplate);


            return View((object)getBlocks(userTemplate));

        }

        // POST: UserTemplates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        // setup game MOVE TO UserGames
        public void SetGame(UserTemplate userTemplate)
        {
            string name = userTemplate.Name;
            int height = userTemplate.Height;
            int width = userTemplate.Width;
            string inCells = userTemplate.Cells;
            var gameOfLife = new GameOfLife(name, height, width, inCells);
            Session["GameOfLife"] = gameOfLife;
            List<GameOfLife> ActiveGames = (List<GameOfLife>)Session["ActiveGames"];
            ActiveGames.Add(gameOfLife);
            Session["ActiveGames"] = ActiveGames;
        }

        public string getBlocks(UserTemplate userTemplate)
        {
            string cellarray = userTemplate.Cells;
            int height = userTemplate.Height;
            int width = userTemplate.Width;
            string name = userTemplate.Name;
            string output = "";

            output += "Name: ";
            output += name;
            output += Environment.NewLine;
            output += "Width: ";
            output += width;
            output += Environment.NewLine;
            output += "Height: ";
            output += height;
            output += Environment.NewLine;
            output += Environment.NewLine;

            string[] Cells = cellarray.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            int Count = height;
            Regex XOxo = new Regex(@"^[XxOo]+$");



            char[][] JAState = new char[height][];



            for (int i = 0; i < height; i++)
            {
                JAState[i] = Cells[i].ToCharArray();

            }



            for (int i = 0; i < JAState.Length; i++)
            {
                char[] innerArray = JAState[i];
                for (int a = 0; a < innerArray.Length; a++)
                {
                    if ((innerArray[a] == 'x') || (innerArray[a] == 'X')) { JAState[i][a] = ' '; }
                    if ((innerArray[a] == 'o') || (innerArray[a] == 'O')) { JAState[i][a] = Globals.aliveCellChar; }
                    output += JAState[i][a].ToString();
                }
                output += Environment.NewLine;
            }

            return (output);

        }

        // GET: Play
        public ActionResult Play(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserTemplate userTemplate = db.UserTemplates.Find(id);
            if (userTemplate == null)
            {
                return HttpNotFound();
            }


            ViewBag.UserID = new SelectList(db.User, "UserID", "Email", userTemplate.UserID);
            return View(userTemplate);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}