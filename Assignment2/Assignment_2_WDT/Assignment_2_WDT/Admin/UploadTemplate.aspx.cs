﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Common;
using System.Web.Security;

namespace Assignment_2_WDT.Admin
{
    public partial class UploadTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect to login if not admin
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        // method for upload button onlick event
        protected void UploadButton_Click(object sender, EventArgs e)
        {
            string inputContent;
            using (StreamReader inputStreamReader = new StreamReader(FileUploadControl.PostedFile.InputStream))
            {
                inputContent = inputStreamReader.ReadToEnd();
            }
            ReadTemplate(inputContent);
        }

        private void ReadTemplate(string inputContent)
        {
            string[] lines = inputContent.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            // gets the filename out of the filepath but should really be user loggged in, sue me.
            // FormsAuthentication.User.
            string name = "test";
            int width;
            int.TryParse(lines[0], out width);
            int height;
            int.TryParse(lines[1], out height);
            string cells;

            // Use StringBuilder to concatenate each cell line into string
            System.Text.StringBuilder sbTemplate = new System.Text.StringBuilder();

            // iterate through cell lines in string array and append to stringbuilder
            for (int i = 2; i < lines.Length; i++)
            {
                sbTemplate.AppendLine(lines[i]);
            }
            cells = sbTemplate.ToString();

            // call Method to use Stored procedure to insert template
            AddTemplateToDB(name, width, height, cells);
        }

        private void AddTemplateToDB(string name, int width, int height, string cells)
        {
            //uid is always admin
            int userID = 1;
            // create new SQL connection
            SqlConnection dbcon = new SqlConnection(GetConString());
            dbcon.Open();
            // create new SQL command, using stored procedure
            SqlCommand stproc = new SqlCommand("dbo.AddTemplate", dbcon);
            stproc.CommandType = CommandType.StoredProcedure;
            // have to send the id with the command so the stored procedure knows what to delete.
            stproc.Parameters.AddWithValue("@UID", userID);
            stproc.Parameters.AddWithValue("@Name", name);
            stproc.Parameters.AddWithValue("@Height", height);
            stproc.Parameters.AddWithValue("@Width", width);
            stproc.Parameters.AddWithValue("@Cells", cells);
            // execute the Stored Procedure and close connection
            stproc.ExecuteNonQuery();
            dbcon.Close();
        }

        // Literally all this method does 
        // is return the Connection string from Web.config, 
        // it's just a long line of code that I didn't want to type again when troubleshooting my bad code
        public string GetConString()
        {
            // just getting the connection string in its own method so I don't have to type this all the time
            return ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        }
    }
}