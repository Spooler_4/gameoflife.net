﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Templates.aspx.cs" Inherits="Assignment_2_WDT.Admin.Templates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <!-- style and header bit comes from Master file so this placeholder just gets filled in by that -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <!-- Main component for a primary content of page -->
    <div class="form-horizontal">
        <h1>Template List</h1>
        <div class="container body-content">
            <%-- This form is populated by the page load method in the code behind,
                 Both Webforms pages operate exactly the same, only differences are in User v UserTemplates fields etc--%>
            <form id="TemplateForm" runat="server">
                <asp:GridView ID="AllTemplatesgv" runat="server" AutoGenerateColumns="False" CssClass="Table" OnRowDeleting="AllTemplatesgv_RowDeleting">
                    <%--Setting up columns to bind DB rows to a DataTable in code behind--%>
                    <Columns>
                        <asp:BoundField DataField="UserTemplateID" HeaderText="TemplateID" ReadOnly="True" />
                        <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" />
                        <asp:BoundField DataField="Height" HeaderText="Height" ReadOnly="True" />
                        <asp:BoundField DataField="Width" HeaderText="Width" ReadOnly="True" />
                        <asp:BoundField DataField="Cells" HeaderText="Cells" ReadOnly="True" />
                        <%-- This is the delete 'button' it asks for confirmation when clicked
                             I used a TemplateField becasue I gave up on getting a normal button to work
                             and CommandField didn't play nice with the onclick confirmation when you ask to delete a record--%>
                        <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                            <ItemTemplate>
                                <span onclick="return confirm('Are you sure want to Delete?')">
                                    <asp:Button ID="DeleteButton" runat="server" CausesValidation="False"
                                        CommandName="Delete" Text="Delete" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </form>
        </div>
    </div>
</asp:Content>
