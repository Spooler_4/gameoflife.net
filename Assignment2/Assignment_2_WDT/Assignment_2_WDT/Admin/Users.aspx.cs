﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Common;
using System.Web.Security;

namespace Assignment_2_WDT.Admin
{
    public partial class Users : System.Web.UI.Page
    {
        // method that runs on page load, currently only really calls the methods that populate the table from DB
        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect to login if not admin
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!this.IsPostBack)
            {
                // Call method to bind the GridView
                BindGridView();
            }
        }

        // method to bind the GridView table with a DataTable which links back to the SQLLocalDB table
        public void BindGridView()
        {
            // create new SQL connection (calls method to grab connection string)
            SqlConnection dbcon = new SqlConnection(GetConString());
            dbcon.Open();
            // create new SQL command, using stored procedure
            SqlCommand stproc = new SqlCommand("dbo.GetUsers", dbcon);
            // crate new SQL Adapter
            SqlDataAdapter dbadapt = new SqlDataAdapter(stproc);
            //create a data table, then fill GridView with the data
            DataTable dt = new DataTable();
            dbadapt.Fill(dt);
            //close connection
            dbcon.Close();
            // Bind the GridView and the datasource
            AllUsersgv.DataSource = dt;
            AllUsersgv.DataBind();
        }

        // Literally all this method does 
        // is return the Connection string from Web.config, 
        // it's just a long line of code that I didn't want to type again when troubleshooting my bad code
        public string GetConString()
        {
            // just getting the connection string in its own method so I don't have to type this all the time
            return ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        }

        // method called by the TemplateField button
        // it calls gets the index and ID of the record to delete and passes it to DeletUserRecord
        protected void AllUsersgv_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // get the ID of the row selected
            string id = AllUsersgv.Rows[e.RowIndex].Cells[0].Text;
            //call delete method on row
            DeleteUserRecord(id);
            //rebind grid to reflect changes made
            BindGridView();
        }

        // Uses a stored procedure to delete a specified DB record
        protected void DeleteUserRecord(string id)
        {
            // create new SQL connection
            SqlConnection dbcon = new SqlConnection(GetConString());
            dbcon.Open();
            // create new SQL command, using stored procedure
            SqlCommand stproc = new SqlCommand("dbo.DeleteUser", dbcon);
            stproc.CommandType = CommandType.StoredProcedure;
            // have to send the id with the command so the stored procedure knows what to delete.
            stproc.Parameters.AddWithValue("@UID", id);
            // execute the Stored Procedure and close connection
            stproc.ExecuteNonQuery();
            dbcon.Close();
        }
    }
}