﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Assignment_2_WDT.Admin.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
    <!-- style and header bit comes from Master file so this placeholder just gets filled in by that -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <!-- Main component for a primary content of page -->
    <div class="form-horizontal">
        <h2>All Users</h2>
        <div class="container body-content">
            <%-- This form is populated by the page load method in the code behind,
                 Both Webforms pages operate exactly the same, only differences are in User v UserTemplates fields etc--%>
            <form id="UserForm" runat="server">
                <asp:GridView ID="AllUsersgv" runat="server" AutoGenerateColumns="False" CssClass="table-condensed" OnRowDeleting="AllUsersgv_RowDeleting">
                    <%--Setting up columns to bind DB rows to a DataTable in code behind--%>
                    <Columns>
                        <asp:BoundField DataField="UserID" HeaderText="UserID" ReadOnly="True" />
                        <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="True" />
                        <asp:BoundField DataField="FirstName" HeaderText="First Name" ReadOnly="True" />
                        <asp:BoundField DataField="LastName" HeaderText="Last Name" ReadOnly="True" />
                        <asp:BoundField DataField="IsAdmin" HeaderText="IsAdmin" ReadOnly="True" />
                        <%-- This is the delete 'button' it asks for confirmation when clicked
                             I used a TemplateField becasue I gave up on getting a normal button to work
                             and CommandField didn't play nice with the onclick confirmation when you ask to delete a record--%>
                        <asp:TemplateField HeaderText="Delete" ShowHeader="False">
                            <ItemTemplate>
                                <span onclick="return confirm('Are you sure you want to Delete?')">
                                    <asp:Button ID="DeleteButton" runat="server" CausesValidation="False"
                                        CommandName="Delete" Text="Delete" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </form>
        </div>
    </div>
</asp:Content>
