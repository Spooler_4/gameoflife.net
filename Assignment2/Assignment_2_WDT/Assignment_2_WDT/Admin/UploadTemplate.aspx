﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="UploadTemplate.aspx.cs" Inherits="Assignment_2_WDT.Admin.UploadTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
 <!-- style and header bit comes from Master file so this placeholder just gets filled in by that -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="form-horizontal">
        <h2>Template Upload</h2>
        <div class="container body-content">
            <form id="UploadForm" runat="server">
                <asp:FileUpload ID="FileUploadControl" runat="server" />
                <asp:Button runat="server" ID="UploadButton" Text="Upload" OnClick="UploadButton_Click" />
            </form>
        </div>
    </div>
</asp:Content>
