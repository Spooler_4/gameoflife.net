﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Security;
using CryptSharp;

namespace Assignment_2_WDT
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // method to validate user
        protected void ValidateUser(object sender, EventArgs e)
        {
            bool Validated = false;
            string userPass = AdminLogin.Password;
            string userName = AdminLogin.UserName;

            // Oh god this is messy I'm so sorry.
            // Create SQL Connection and command, set command to stored procedure,
            // set type and add paramaters, only expecting one result so using scalar becasue nothing else works
            // READ: I gave up.
            // probably should wrap this in disposables... 
            SqlConnection dbcon = new SqlConnection(GetConString());
            SqlCommand command = new SqlCommand("dbo.GetPass");
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", userName);
            command.Connection = dbcon;
            dbcon.Open();
            string loginPassword = (string)command.ExecuteScalar();
            dbcon.Close();

            // dont check validity if the password wasnt returned from DB
            if (!string.IsNullOrEmpty(loginPassword))
            {
                // check to see if the passwords match
                if (Crypter.CheckPassword(userPass, loginPassword))
                {
                    Validated = true;
                }
            }

            
            if (Validated)
            {
                FormsAuthentication.RedirectFromLoginPage(AdminLogin.UserName, AdminLogin.RememberMeSet);
            }

        }

        // Literally all this method does 
        // is return the Connection string from Web.config, 
        // it's just a long line of code that I didn't want to type again when troubleshooting my bad code
        public string GetConString()
        {
            // just getting the connection string in its own method so I don't have to type this all the time
            return ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        }
    }
}