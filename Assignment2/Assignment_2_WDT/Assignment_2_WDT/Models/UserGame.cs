//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Assignment_2_WDT.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public partial class UserGame
    {
        private string userID;

        public UserGame(string userID, string name, int height, int width, string cells)
        {
            this.userID = userID;
            Name = name;
            Height = height;
            Width = width;
            Cells = cells;
        }

        public UserGame(int userGameID, string userID, string name, int height, int width, string cells)
        {
            UserGameID = userGameID;
            this.userID = userID;
            Name = name;
            Height = height;
            Width = width;
            Cells = cells;
        }

        public int UserGameID { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string Cells { get; set; }

        public virtual User User { get; set; }

        // this method is used to display the Cells as unicode characters instead of the XO string
        public string convertCells
        {
            get
            {
                string cellarray = Cells;
                int height = Height;
                int width = Width;
                string name = Name;
                string output = "";

                string[] CellsArray = cellarray.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                int Count = height;
                Regex XOxo = new Regex(@"^[XxOo]+$");
                char[][] JAState = new char[height][];
                for (int i = 0; i < height; i++)
                {
                    JAState[i] = CellsArray[i].ToCharArray();
                }
                
                for (int i = 0; i < JAState.Length; i++)
                {
                    char[] innerArray = JAState[i];
                    for (int a = 0; a < innerArray.Length; a++)
                    {
                        if ((innerArray[a] == 'x') || (innerArray[a] == 'X')) { JAState[i][a] = ' '; }
                        if ((innerArray[a] == 'o') || (innerArray[a] == 'O')) { JAState[i][a] = Globals.aliveCellChar; }
                        output += JAState[i][a].ToString();
                    }
                    output += Environment.NewLine;
                }

                return (output);
            }

        }
    }
}
