﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Assignment_2_WDT.Controllers;

namespace Assignment_2_WDT
{

    public class GameOfLife
    {
        public string Name { get; set; }
        public int Height { get; set; }
        private string currentCell { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }

        // globals
        public static class Globals
        {

            public const char aliveCellChar = '\u2588';

            public const string aliveCellString = "\u2588";
        }

        // end globals
        public enum Cell
        {
            Alive,
            Dead
        }

        public GameOfLife(string name, int height, int width, Cell[][] Cells)
        {
            Name = name;
            Height = height;
            Width = width;
            this.Cells = Cells;
        }

        public GameOfLife(string name, int height, int width, string cellString)
        {
            Name = name;
            Height = height;
            Width = width;
            Cells = convertToCells(Height, Width, cellString);
        }

        internal string ToPlainString()
        {

            string convertedString;
            // Use StringBuilder to concatenate each attribute into string
            System.Text.StringBuilder sbTemplate = new System.Text.StringBuilder();
            // iterate through Cells array and append to stringbuilder
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if (Cells[i][j] == Cell.Dead)
                    {
                        sbTemplate.Append('X');
                    }
                    else
                    {
                        sbTemplate.Append('O');
                    }
                }
                sbTemplate.AppendLine();
            }

            convertedString = sbTemplate.ToString();
            return convertedString;
        }

        private Cell[][] convertToCells(int height, int width, string cellString)
        {
            Cell[][] NewCells = new Cell[height][];

            string[] CellsArray = cellString.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < CellsArray.Length; i++)
            {
                Char[] row = CellsArray[i].ToCharArray();
                NewCells[i] = new Cell[Width];
                for (int j = 0; j < row.Length; j++)
                {
                    if (row[j] == 'X' || row[j] == 'x')
                    {
                        NewCells[i][j] = Cell.Dead;
                    }
                    else if (row[j] == 'O' || row[j] == 'o')
                    {
                        NewCells[i][j] = Cell.Alive;
                    }
                }
            }
            return NewCells;
        }

        public override string ToString()
        {
            string convertedString;
            // Use StringBuilder to concatenate each attribute into string
            System.Text.StringBuilder sbTemplate = new System.Text.StringBuilder();
            // iterate through Cells array and append to stringbuilder
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if (Cells[i][j] == Cell.Dead)
                    {
                        sbTemplate.Append(' ');
                    }
                    else
                    {
                        sbTemplate.Append('\u2588');
                    }
                }
                sbTemplate.AppendLine();
            }

            convertedString = sbTemplate.ToString();
            return convertedString;
        }

        public void TakeTurn()
        {
            // create transitionary array to copy cells into
            Cell[][] newState = new Cell[Height][];

            // copy all cells
            for (int i = 0; i < Height; i++)
            {
                newState[i] = new Cell[Width];
                for (int j = 0; j < Width; j++)
                {
                    newState[i][j] = Cells[i][j];
                }
            }

            int adjAlive;
            // for each cell get neighbour alive count and then
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    // less than two dies
                    adjAlive = GetLivingNeighbours(i, j);
                    if (Cells[i][j] == Cell.Alive)
                    {
                        if (adjAlive < 2)
                        {
                            newState[i][j] = Cell.Dead;
                        }

                        // 2 or 3 lives
                        if (adjAlive == 2 || adjAlive == 3)
                        {
                            newState[i][j] = Cell.Alive;
                        }

                        // over 3 dies
                        if (adjAlive > 3)
                        {
                            newState[i][j] = Cell.Dead;
                        }
                    }
                    else if (Cells[i][j] == Cell.Dead)
                    {
                        // if dead over 3 becomes alive
                        if (adjAlive == 3)
                        {
                            newState[i][j] = Cell.Alive;
                        }
                    }
                }
            }

            //copy newState back into game
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Cells[i][j] = newState[i][j];
                }
            }
        }

        // this method returns Cells adjacent to the cell at x, y ref NOT a list of Australian Soap stars
        public int GetLivingNeighbours(int x, int y)
        {
            int count = 0;
            int lowerBound = 0;
            int xUpperBound = (Cells.Length - 1);
            int yUpperBound = (Cells[0].Length - 1);
            // top left
            if (x != lowerBound)
            {
                if (y != lowerBound)
                {
                    if (Cells[x - 1][y - 1].Equals(Cell.Alive))
                    {
                        count++;
                    }
                }
                // top middle
                if (Cells[x - 1][y].Equals(Cell.Alive))
                {
                    count++;
                }
                // top right
                if (y != yUpperBound)
                {
                    if (Cells[x - 1][y + 1].Equals(Cell.Alive))
                    {
                        count++;
                    }
                }
            }
            // left
            if (y != lowerBound)
            {
                if (Cells[x][y - 1].Equals(Cell.Alive))
                {
                    count++;
                }
            }
            // right
            if (y != yUpperBound)
            {
                if (Cells[x][y + 1].Equals(Cell.Alive))
                {
                    count++;
                }
            }
            // bottom left
            if (x != xUpperBound)
            {
                if (y != lowerBound)
                {
                    if (Cells[x + 1][y - 1].Equals(Cell.Alive))
                    {
                        count++;
                    }
                }
                // bottom middle
                if (Cells[x + 1][y].Equals(Cell.Alive))
                {
                    count++;
                }
                // bottom right
                if (y != yUpperBound)
                {
                    if (Cells[x + 1][y + 1].Equals(Cell.Alive))
                    {
                        count++;
                    }
                }
            }
            return count;
        }

    }
}
